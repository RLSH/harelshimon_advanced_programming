#include "Rectangle.h"

/*
create ne rectangle
*/
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : _leftUpCorner(a), Polygon(type, name), _rightDownCorner((a.getX() + length), (a.getY() + width))
{
	//if the rect len or width is 0 or smaller throw error
	if (length <= 0 || width <= 0)
	{
		throw(invalid_argument("the sides of the rectangle must be bigger than 0!"));
	}

	this->_length = length;
	this->_width = width;
}

//d'tor
myShapes::Rectangle::~Rectangle()
{

}

//alculate the area
double myShapes::Rectangle::getArea() const
{
	return (this->_length * this->_width);
}

//calxulate the perimeter
double myShapes::Rectangle::getPerimeter() const
{
	return (2 * (this->_length + this->_width));
}

//move the rect
void myShapes::Rectangle::move(const Point& other)
{
	this->_leftUpCorner += other;
}

//print the rect's details
void myShapes::Rectangle::printDetails()
{
	cout << "the point of the corner: (" << _leftUpCorner.getX() << ", " << _leftUpCorner.getY() << ")" << endl;
	cout << "the length: " << _length << endl;
	cout << "the width: " << _width << endl;
	cout << "the area is: " << getArea() << ", and the perimeter is: " << getPerimeter() << endl;
}

void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_leftUpCorner.getX(), _leftUpCorner.getY(),
		_leftUpCorner.getX() + _length, _leftUpCorner.getY() + _width, WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_rectangle(_leftUpCorner.getX(), _leftUpCorner.getY(),
		_leftUpCorner.getX() + _length, _leftUpCorner.getY() + _width, BLACK, 100.0f).display(disp);
}


