#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();
	void mainMenu();

private:
	
	Shape* addNewShape();
	Shape* createCircle();
	Shape* createArrow();
	Shape* createTriangle();
	Shape* createRectangle();
	int clearAllDraws(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void modify(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void printToBoard(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

