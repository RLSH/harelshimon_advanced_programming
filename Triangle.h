#pragma once
#include "Polygon.h"

#define PI 3.141592653

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();

	virtual double getArea() const;
	virtual double getPerimeter() const;
	virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual void move(const Point& moving); // add the Point to all the points of shape
	void printDetails();
	

	virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);

private:
	Point _p1;
	Point _p2;
	Point _p3;
	// override functions if need (virtual + pure virtual)
};