#include "Arrow.h"

/*
this is the c'tor of the arrow obj
*/
Arrow::Arrow(const Point& a, const Point& b, const string& type, const string& name) : _p1(a), _p2(b), Shape(name, type)
{

}

Arrow::~Arrow()
{

}

//arrow have no area
double Arrow::getArea() const
{
	return 0;
}

//the perimeter is the distance
double Arrow::getPerimeter() const
{
	return _p1.distance(_p2);
}

/*
move the 2 points of the arrow
*/
void Arrow::move(const Point& moving) 
{
	_p1 = Point(_p1.getX() + moving.getX(), _p1.getY() + moving.getY());
	_p2 = Point(_p2.getX() + moving.getX(), _p2.getY() + moving.getY());
	// add the Point to all the points of shape
}

/*
print detalis obout the arrow
*/
void Arrow::printDetails()
{
	cout << "_p1: (" << _p1.getX() << ", " << _p1.getY() << ")" << endl;
	cout << "_p2: (" << _p2.getX() << ", " << _p2.getY() << ")" << endl;
	cout << "the area is: " << getArea() << ", and the perimeter is: " << getPerimeter() << endl;
}

//this function draw that arrow on the board
void Arrow::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char RED[] = { 255, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), RED, 100.0f).display(disp);
}

//clear that arrow from the board
void Arrow::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };

	board.draw_arrow(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(), BLACK, 100.0f).display(disp);
}


