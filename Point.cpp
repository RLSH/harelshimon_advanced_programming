#include "Point.h"

/*
this is point obj
*/
Point::Point(double x, double y)
{
	//reset x and y
	this->_x = x;
	this->_y = y;
}

/*
copy c'tor. do deep copy
*/
Point::Point(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

Point::~Point()
{

}

/*
operator + function. add this point and the input point and return new point
*/
Point Point::operator+(const Point& other) const
{
	double x = other.getX() + this->_x;
	double y = other.getY() + this->_y;
	Point pointsSum(x, y);

	return pointsSum;
}

/*
operator+= function. add this point and the input point into this point
*/
Point& Point::operator+=(const Point& other)
{
	this->_x += other.getX();
	this->_y += other.getY();

	return *this;
}

/*
operator = function. do deep copy to the input point
*/
Point Point::operator=(const Point& other)
{
	this->_x = other.getX();
	this->_y = other.getY();


	return *this;
}

/*
return the x 
*/
double Point::getX() const
{
	return this->_x;
}

/*
return the y
*/
double Point::getY() const
{
	return this->_y;
}

/*
calculate the distance between this point and input point and return it
*/
double Point::distance(const Point& other) const
{
	return sqrt(pow(abs(this->_x - other.getX()), 2) + pow(abs(this->_y - other.getY()), 2));
}
