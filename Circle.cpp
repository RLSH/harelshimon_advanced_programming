#include "Circle.h"

/*
print the circle to the board
*/
void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

/*
clean the draw from the board
*/
void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}

/*
this is the c'tor function of the obj
*/
Circle::Circle(const Point& center, double radius, const string& type, const string& name) :_center(center), _radius(radius), Shape(name, type)
{
}

//d'tor
Circle::~Circle()
{

}


/*
return the center pointer 
*/
const Point& Circle::getCenter() const
{
	return this->_center;
}

/*
return the radius length
*/
double Circle::getRadius() const
{
	return this->_radius;
}

//return the area of the circle
double Circle::getArea() const
{
	return PI * pow(this->_radius, 2);
}

//return the perimeter of this circle
double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius;
}

//move the center
void Circle::move(const Point& other)
{
	this->_center = Point((other.getX() + this->_center.getX()), (other.getY() + this->_center.getY()));
}


void Circle::printDetails()
{
	cout << "the center is: (" << _center.getX() << ", " << _center.getY() << ")" << endl;
	cout << "the radius is: " << getRadius() << endl;
	cout << "the area is: " << getArea() << ", and the perimeter is: " << getPerimeter() << endl;
}

