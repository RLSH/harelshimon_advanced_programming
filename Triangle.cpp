#include "Triangle.h"

/*
this is the c'tor of the triangle
*/
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : _p1(a), _p2(b), _p3(c), Polygon(type, name)
{
	//if all the points are in line throw error
	if ((_p1.getX() == _p2.getX() && _p2.getX() == _p3.getX()) || (_p1.getY() == _p2.getY() && _p2.getY() == _p3.getY()))
	{
		throw(invalid_argument("all the points are in the same line!"));
	}
}

Triangle::~Triangle()
{

}

/*
this function caculate the area of the triangle
*/
double Triangle::getArea() const
{
	double deg = 0;
	//all the sides len
	double a = _p1.distance(_p2);
	double b = _p2.distance(_p3);
	double c = _p3.distance(_p1);

	//caculate the deg with the cos formula
	deg = acos((pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2 * a * b)) * 180 / PI;

	//the area with sin
	return (a * b * sin(deg)) / 2;
}

/*
this function calculate the perimeter of this
*/
double Triangle::getPerimeter() const
{
	double sum = 0;

	sum += _p1.distance(_p2);
	sum += _p2.distance(_p3);
	sum += _p3.distance(_p1);

	return sum;
}

//move all the points of the triangle
void Triangle::move(const Point& moving)
{
	_p1 = Point((_p1.getX() + moving.getX()), (_p1.getY() + moving.getY()));
	_p2 = Point((_p2.getX() + moving.getX()), (_p2.getY() + moving.getY()));
	_p3 = Point((_p3.getX() + moving.getX()), (_p3.getY() + moving.getY()));
	// add the Point to all the points of shape
}

/*
this function print the details of the triangle
*/
void Triangle::printDetails()
{
	cout << "_p1: (" << _p1.getX() << ", " << _p1.getY() << ")" << endl;
	cout << "_p2: (" << _p2.getX() << ", " << _p2.getY() << ")" << endl;
	cout << "_p3: (" << _p3.getX() << ", " << _p3.getY() << ")" << endl;
	cout << "the area is: " << getArea() << ", and the perimeter is: " << getPerimeter() << endl;
}

//draw the triangle
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(),
		_p3.getX(), _p3.getY(), GREEN, 100.0f).display(disp);
}

//clear it
void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_p1.getX(), _p1.getY(),
		_p2.getX(), _p2.getY(),
		_p3.getX(), _p3.getY(), BLACK, 100.0f).display(disp);
}
