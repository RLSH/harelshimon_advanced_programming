#include "Menu.h"

//c'tor
Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");

}

//d'tor
Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}

//the main function
void Menu::mainMenu()
{
	int option = 0;
	int counter = 0;

	//array of all the Shapes
	vector<Shape*> allShapesArray;

	//while the user dont want to stop
	while (option != 3)
	{
		cout << "Enter 0 to add a new shape." << endl;
		cout << "Enter 1 to modify or get information from a current shape." << endl;
		cout << "Enter 2 to delete all of the shapes." << endl;
		cout << "Enter 3 to exit." << endl;

		//user enter an option
		cin >> option;

		//if wrong input the usre enter again
		if (option > 3 || option < 0)
		{
			cout << "error! option are only between 0 - 3 !" << endl;
		}

		system("test&cls");

		if (option == 0)
		{
			//add Shape
			allShapesArray.push_back(addNewShape());
		}
		else if (option == 1)
		{
			//modify
			modify(allShapesArray, *_disp, *_board);
		}
		else if (option == 2)
		{
			//clear all draws
			clearAllDraws(allShapesArray, *_disp, *_board);
			system("test&cls");
		}

		//print the board
		printToBoard(allShapesArray, *_disp, *_board);
	}
}

/*
add new Shape
*/
Shape* Menu::addNewShape()
{
	int option = 0;

	cout << "Enter 0 to add a circle." << endl;
	cout << "Enter 1 to add an arrow." << endl;
	cout << "Enter 2 to add a triangle." << endl;
	cout << "Enter 3 to add a rectangle." << endl;

	//the user choose the Shape
	cin >> option;

	while (option < 0 || option > 3)
	{
		cout << "option can be between 0 - 3 ! enter again." << endl;
		cin >> option;
	}

	system("test&cls");

	if (option == 0)
	{
		//create circle
		return createCircle();
	}
	else if (option == 1)
	{
		//create arrow
		return createArrow();
	}
	else if (option == 2)
	{
		//create triangle
		return createTriangle();
	}
	else if (option == 3)
	{
		//create rectangle
		return createRectangle();
	}
}


/*
this fnuction create circle
*/
Shape* Menu::createCircle()
{
	double x = 0;
	double y = 0;
	double radius = 0;
	string nameOfShape = "";
	
	//user enter info
	cout << "Please enter X: " << endl;
	cin >> x;
	cout << "Please enter Y: " << endl;
	cin >> y;
	Point center(x, y);

	cout << "Please enter radius: " << endl;
	cin >> radius;
	cout << "Please enter name of shape: " << endl;
	getline(cin, nameOfShape);
	getline(cin, nameOfShape);

	system("test&cls");

	//create the circle
	Shape* cir = new Circle(center, radius, "circle", nameOfShape);

	return cir;
}


/*
this functio create an arrow
*/
Shape* Menu::createArrow()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	string name = "";

	//the user enter info
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;

	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;

	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;

	cout << "Enter the name of the shape:" << endl;
	getline(cin, name);
	getline(cin, name);

	system("test&cls");

	//create the arrow
	Shape* arrow = new Arrow(Point(x1, y1), Point(x2, y2), "arrow", name);

	return arrow;
}


/*
this funciton create a triangle
*/
Shape* Menu::createTriangle()
{
	double x1 = 0;
	double y1 = 0;
	double x2 = 0;
	double y2 = 0;
	double x3 = 0;
	double y3 = 0;
	string name = "";


	//the user enter info
	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;

	cout << "Enter the Y of point number: 1" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;

	cout << "Enter the Y of point number: 2" << endl;
	cin >> y2;

	cout << "Enter the X of point number: 3" << endl;
	cin >> x3;

	cout << "Enter the Y of point number: 3" << endl;
	cin >> y3;

	cout << "Enter the name of the shape:" << endl;
	getline(cin, name);
	getline(cin, name);

	system("test&cls");

	//create a triangle
	Shape* triang = new Triangle(Point(x1, y1), Point(x2, y2), Point(x3, y3), "triangle", name);

	return triang;
}

/*
this function create rectangle
*/
Shape* Menu::createRectangle()
{
	double leftCornerX = 0;
	double leftCornerY = 0;
	double length = 0;
	double width = 0;
	string name = "";

	//the user enter info
	cout << "Enter the X of the to left corner: " << endl;
	cin >> leftCornerX;
	cout << "Enter the Y of the top left corner: " << endl;
	cin >> leftCornerY;
	cout << "Please enter the length of the shape: " << endl;
	cin >> length;
	cout << "Please enter the width of the shape: " << endl;
	cin >> width;
	cout << "Enter the name of the shape: " << endl;
	getline(cin, name);
	getline(cin, name);

	system("test&cls");

	//create rect
	Shape* rect = new myShapes::Rectangle(Point(leftCornerX, leftCornerY), length, width, "rectangle", name);

	return rect;
}

/*
this function chould clear all draws
*/
int Menu::clearAllDraws(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	int i = 0;
	int stop = allShapesArray.size();

	while (i < stop)
	{
		allShapesArray[0]->clearDraw(disp, board);
		allShapesArray.erase(allShapesArray.begin());

		i++;
	}

	system("test&cls");

	return 0;
}

/*
this is the modify function
*/
void Menu::modify(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	int i = 0;
	int index = 0;
	int input = 0;
	double XMoving = 0;
	double YMoving = 0;

	if (allShapesArray.size() > 0)
	{
		//print all the array content
		while (i < allShapesArray.size())
		{
			cout << "Enter " << i << " for " + allShapesArray[i]->getName() + '(' + allShapesArray[i]->getType() + ')' << endl;
			i++;
		}
		//the user enter obj
		cin >> index;

		if (index < allShapesArray.size() && index >= 0)
		{
			cout << "Enter 0 to move the shape" << endl;
			cout << "Enter 1 to get its details." << endl;
			cout << "Enter 2 to remove the shape." << endl;

			//the user enter method
			cin >> input;

			//moving
			if (input == 0)
			{
				cout << "Please enter the X moving scale:" << endl;
				cin >> XMoving;
				cout << "Please enter the Y moving scale:" << endl;
				cin >> YMoving;


				allShapesArray[index]->clearDraw(disp, board);
				allShapesArray[index]->move(Point(XMoving, YMoving));
			}
			//print detals
			else if (input == 1)
			{
				cout << allShapesArray[index]->getType() << ' ' << allShapesArray[index]->getName() << endl;
				allShapesArray[index]->printDetails();
				system("PAUSE");
				system("test&cls");
			}
			//delete from the array
			else if (input == 2)
			{
				allShapesArray[index]->clearDraw(disp, board);
				allShapesArray.erase(allShapesArray.begin() + index);
			}
		}
	}
}

/*
this function print all the shapes from the array to the board
*/
void Menu::printToBoard(vector<Shape*>& allShapesArray, cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	int i = 0;

	for (i = 0; i < allShapesArray.size(); i++)
	{
		allShapesArray[i]->draw(disp, board);
	}
}

