#include "Shape.h"

/*
the basic obj 
*/
Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}

/*
print te details of the shape

void Shape::printDetails() const
{
	cout << "the name of the shape: " + _name << endl;
	cout << "the type of the shape: " + _type << endl;
}
*/

/*
print the type of the shape
*/
string Shape::getType() const
{
	return this->_type;
}

/*
ptinr the name of the shape
*/
string Shape::getName() const
{
	return this->_name;
}
